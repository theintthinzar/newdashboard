<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Sale;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\ShopStock;
use App\Warehouse;
use DB;

class AdminController extends Controller
{
    public function index(){

        $warehouses = Warehouse::count();

		$sale= Sale::count();
		$sales = $sale/100;

        $customers = Customer::count();
        $shopstocks = ShopStock::count();
        $invoices = Invoice::count();
        $items = Item::count();

        for($i=1; $i<13; $i++)
        {
        	$whcountbymonth[$i] = Warehouse::whereMonth('createtime', '=', $i )->count();
        }

        for($i=1; $i<13; $i++)
        {
        	$salcountbymonth[$i] = Sale::whereMonth('udt', '=', $i )->count();
        }

        for($i=1; $i<13; $i++)
        {
        	$invcountbymonth[$i] = Invoice::whereMonth('udt', '=', $i )->count();
        }

        for($i=1; $i<13; $i++)
        {
        	$stockcountbymonth[$i] = ShopStock::whereMonth('cdt', '=', $i )->count();
        }


        return view('admin.index',compact('warehouses','sales','customers','shopstocks','invoices','items', 'whcountbymonth', 'salcountbymonth', 'invcountbymonth', 'stockcountbymonth'));
    }
}
