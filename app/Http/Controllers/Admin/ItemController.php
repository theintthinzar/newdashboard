<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index(){
        $items = Item::all();
        return view('admin.item.index',compact('items'));
    }
}
