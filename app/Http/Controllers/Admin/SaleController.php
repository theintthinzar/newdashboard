<?php

namespace App\Http\Controllers\Admin;

use App\Sale;
use App\SaleDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SaleController extends Controller
{
    public function index(){
        $sales = Sale::all();
        return view('admin.sale.index',compact('sales'));
    }

    public function totalQty()
    {
        // for($i=1; $i<13; $i++)
        // {
        //     $data[$i] = DB::table('tb_salesdetail')
        //                 ->join('tb_item', 'tb_salesdetail.iid', '=', 'tb_item.id')
        //                 ->select(
        //                     DB::raw("SUM(tb_salesdetail.qty) as total_qty"),
        //                     'tb_item.name')
        //                 ->groupBy(DB::raw("month(udt)"))
        //                 ->whereMonth('tb_salesdetail.udt', '=', $i )
        //                 ->get();
        // }

        // //dd($data);
    	return view('admin.sale.totalQty');
    }

    public function byMonth(Request $request)
    {
        $month = $request->month;

        $data = DB::table('tb_item')
                ->join('tb_salesdetail', 'tb_item.id', '=', 'tb_salesdetail.iid')
                ->select('tb_item.name', DB::raw('sum(tb_salesdetail.qty) as total_qty'))
                ->whereMonth('tb_salesdetail.udt', '=', $month)
                ->groupBy('tb_item.id', 'tb_item.name')
                ->get();
        //dd($data);

        return view('admin.sale.month', compact('data'));
    }
}
