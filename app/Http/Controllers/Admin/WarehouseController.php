<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    public function index(){
        $warehouses = Warehouse::all();
        return view('admin.warehouse.index',compact('warehouses'));
    }
}
