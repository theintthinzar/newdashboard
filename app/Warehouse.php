<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = 'tb_warehouse';

    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';
}
