@extends('admin/sale/totalQty')

@section('month')

<div class="row justify-content-md-center">
	<h3>Total Quantity of Sale Item Table</h3>
</div>

<div class="row justify-content-md-center">
<table class="table table-hover" align="center">
  <thead>
    <tr>
      <th scope="col-md">No</th>
      <th scope="col-md">Item Name</th>
      <th scope="col-md">Total Quatity</th>
    </tr>
  </thead>
  <tbody>
	@foreach($data as  $key =>$row)
    <tr class="table-active">
    	<td>{{++$key}}</td>
        <td>{{$row->name}}</td>
      	<td>{{$row->total_qty}}</td>
    </tr>
	@endforeach
</tbody>
</table>
</div>

@endsection