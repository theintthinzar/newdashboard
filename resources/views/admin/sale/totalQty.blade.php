@extends('admin/layouts/app')
@section('main-content')

<div class="container">
	<div class="row justify-content-around">
		<form method ="POST" action="/admin/sale/totalqty/month" >
			@csrf
    		<div class="col-8">
      			<label for="month">Select Month</label>
		      	<select class="form-control btn btn-info" name="month">
			        <option value="1">January</option>
			        <option value="2">February</option>
			        <option value="3">March</option>
			        <option valaue="4">April</option>
			        <option value="5">May</option>
			        <option value="6">June</option>
			        <option value="7">July</option>
			        <option value="8">August</option>
			        <option value="9">September</option>
			        <option value="10">October</option>
			        <option value="11">November</option>
			        <option value="12">December</option>
		      	</select>
    		</div><br>
		    <div class="col-4">
		      <button class="btn btn-warning" type="submit">Submit</button>
		    </div>
		</form>
	</div>
	@yield('month')

</div>

@endsection