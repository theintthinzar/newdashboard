<?php

use App\Invoice;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/admin',function(){
//     return view('admin.index');
// })->name('admin');

Route::get('/admin','Admin\AdminController@index')->name('admin');

Route::get('/admin/tables',function(){
    return view('admin.tables.data');
})->name('admin.tables');

Route::get('/admin/forms',function(){
    return view('admin.forms.general');
})->name('admin.forms');

Route::get('admin/invoice',function(){
    return Invoice::count();
});

Route::get('/admin/customer','Admin\CustomerController@index')->name('customer');
Route::get('/admin/sale','Admin\SaleController@index')->name('sale');
Route::get('/admin/warehouse','Admin\WarehouseController@index')->name('warehouse');
Route::get('/admin/item','Admin\ItemController@index')->name('item');

Route::get('/admin/sale/totalqty', 'Admin\SaleController@totalQty');
Route::post('/admin/sale/totalqty/month', 'Admin\SaleController@byMonth');

Route::get('/git',function(){
    echo "Git Testing";
});
